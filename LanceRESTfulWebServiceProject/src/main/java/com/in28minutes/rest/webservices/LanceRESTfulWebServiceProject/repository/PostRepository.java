package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.repository;

import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.Post;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.User;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PostRepository {

    private static HashMap<String, Post> posts = new HashMap<>();

//    static {
//        posts.put("1", new Post("1","Hey Friends: Lance", new Date()));
//        posts.put("2", new Post("2","Hey Friends: Craig", new Date()));
//        posts.put("3", new Post("3","Hey Friends: Wakanda", new Date()));
//    }

    public List<Post> getPostsByUser(List<String> userPostIds) {
        List<Post> userPosts = new ArrayList<Post>();
        for(String postId : userPostIds){
            userPosts.add(getPost(postId));
        }
        return userPosts;
    }

    public Post getPost(String postId){
        return posts.get(postId);
    }

    public Post addPost(Post post){
        this.posts.put(post.getPostID(), post);
        return post;
    }

//    public int getTotalPostCount(){
//        return posts.size();
//    }
}
