package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.repository;

import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.Post;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.User;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class UserRepository {
    private static List<User> users = new ArrayList<>();

    private static int userCount = 3;

//    static {
//        users.add(new User("1", "Lance", new Date(), Arrays.asList("1")));
//        users.add(new User("2", "Craig", new Date(), Arrays.asList("2")));
//        users.add(new User("3", "Wakanda", new Date(), Arrays.asList("3")));
//    }

    public List<User> getAllUsers(){
        return users;
    }

    public int getUserCount(){
        return userCount;
    }

    public User saveNewUser(User user){
        users.add(user);
        return user;
    }

    public User findOneUser(String id){
        for(User user:users){
            if(user.getId().equals(id)){
                return user;
            }
        }
        return null;
    }

    public User updateUser(User updatedUser){
        for(int i = 0; i < users.size(); i++) {
            if (users.get(i).getId().equals(updatedUser.getId())) {
                users.set(i, updatedUser);
                return users.get(i);
            }
        }
        return null;
    }

    public User deleteUser(String userId) {
        Iterator<User> iterator = users.iterator();
        while(iterator.hasNext()){
            User user = iterator.next();
            if(user.getId().equals(userId)){
                iterator.remove();
                return user;
            }
        }
        return null;
    }
}
