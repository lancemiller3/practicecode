package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.repository;

import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserJPARepository extends JpaRepository<User, String> {


}
