package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.helloworld;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

//Controller
@RestController
public class HelloWorldController {

    //GET
    //URI - /hello-world

    //method - "Hello World"
    //@RequestMapping(method= RequestMethod.GET, path="/hello-world") //Alternative way to find the mapping: /hello-world
    @GetMapping(path="/hello-world")
    public String helloWorld(){
        return "Hello World";
    }

    @GetMapping(path="/hello-world-bean")//without HwlloWorldBean's message getter method this would not work
    public List<HelloWorldBean> helloWorldBean(){
        return Arrays.asList(new HelloWorldBean("Hello World"), new HelloWorldBean("Hello World2"), new HelloWorldBean("Hello World3"));
    }

    @GetMapping(path="/hello-world/path-variable/{name}")
    public HelloWorldBean helloWorldPathVariable(@PathVariable String name){
        return new HelloWorldBean(String.format("Hello World %s", name));
    }
}
