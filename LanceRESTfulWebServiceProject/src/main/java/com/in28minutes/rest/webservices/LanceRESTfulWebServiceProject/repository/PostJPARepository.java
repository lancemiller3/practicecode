package com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.repository;

import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.Post;
import com.in28minutes.rest.webservices.LanceRESTfulWebServiceProject.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface PostJPARepository extends JpaRepository<Post, String> {

    //@Query("SELECT p FROM POST p WHERE p.USER_ID = :userId")
    List<Post> findByUser_Id(String userId);

}
