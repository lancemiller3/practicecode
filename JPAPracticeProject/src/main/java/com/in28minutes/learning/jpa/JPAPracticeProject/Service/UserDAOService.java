package com.in28minutes.learning.jpa.JPAPracticeProject.Service;

import com.in28minutes.learning.jpa.JPAPracticeProject.Entity.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@Repository
@Transactional //each method will be involved in a transaction
public class UserDAOService {

    @PersistenceContext
    private EntityManager entityManager;

    public Long inset(User user){
        entityManager.persist(user);
        return user.getId();

    }
}
