package com.in28minutes.learning.jpa.JPAPracticeProject;

import com.in28minutes.learning.jpa.JPAPracticeProject.Entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {


}
