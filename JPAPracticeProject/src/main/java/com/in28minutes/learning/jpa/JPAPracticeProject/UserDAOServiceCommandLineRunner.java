package com.in28minutes.learning.jpa.JPAPracticeProject;

import com.in28minutes.learning.jpa.JPAPracticeProject.Entity.User;
import com.in28minutes.learning.jpa.JPAPracticeProject.Service.UserDAOService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class UserDAOServiceCommandLineRunner implements CommandLineRunner {

    @Autowired
    private UserDAOService userDAOService;

    private static final Logger log = LoggerFactory.getLogger(UserDAOServiceCommandLineRunner.class);

    /**
     * Callback used to run the bean.
     *
     * @param args incoming main method arguments
     * @throws Exception on error
     */
    @Override
    public void run(String... args) throws Exception {
        User user = new User("Jack", "Admin");
        Long insert = userDAOService.inset(user);
        log.info("New User is Created " + user);
    }
}

//
