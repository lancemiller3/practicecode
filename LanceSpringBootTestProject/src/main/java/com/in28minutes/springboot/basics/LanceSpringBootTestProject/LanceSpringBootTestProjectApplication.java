package com.in28minutes.springboot.basics.LanceSpringBootTestProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

//spring boot looks at
	//a)framweorks available on the Classpath
	//b)existing configuration for the application
//based on these, Spring Boot provides basic configuration needed to configure the application with these frameworks
//this is called Auto Configuration
//autoconfiguration can also be conditional with @ConditionalOnClass and @ConditionalOnMissingBean

@SpringBootApplication//this is spring context, enables auto configuration, enable auto scan to package ^^^
public class LanceSpringBootTestProjectApplication {

	public static void main(String[] args) {

		ApplicationContext appContext = SpringApplication.run(LanceSpringBootTestProjectApplication.class, args);

		for(String name : appContext.getBeanDefinitionNames()){
			System.out.println(name);
		}
	}

}
